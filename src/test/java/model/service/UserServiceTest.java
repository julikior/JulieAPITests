package model.service;

import static model.UserRequest.userEmail;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static users.UserEndpoint.randomGender;
import static users.UserEndpoint.randomStatus;

import listeners.TestListener;
import mock.UserRepository;
import model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.testng.annotations.Listeners;
import users.UserEndpoint;

@Listeners({TestListener.class})
@ExtendWith(MockitoExtension.class)
public class UserServiceTest {

  @InjectMocks UserServiceImpl userSample;

  @Mock UserRepository userRepository;
  @Mock EmailVerificationServiceImpl emailVerificationService;

  public String firstName;
  public String userEmail;

  @BeforeEach
  void init() {
    firstName = "firstName_" + System.currentTimeMillis();
    userEmail = "test.email_" + System.currentTimeMillis() + "@15ce.com";
  }

  @Test
  @DisplayName("Can create User")
  public void testCreateUser_withUserDetails() throws IllegalAccessException {

    Mockito.when(userRepository.save(any(User.class))).thenReturn(true);

    User user = userSample.createUser(firstName, userEmail, randomGender(), randomStatus());
    assertNotNull(user);
    assertEquals(user.getName(), firstName);
    assertNotNull(user, "The createUser should not have return null");
    Mockito.verify(userRepository).save(any(User.class));
  }

  @Test
  @DisplayName("Attempt to create user with empty name")
  public void test_cannotCreateUserWithEmptyName() throws IllegalAccessException {
    String firstName = "";
    IllegalArgumentException thrown =
        assertThrows(
            IllegalArgumentException.class,
            () -> {
              userSample.createUser(firstName, userEmail, randomGender(), randomStatus());
            },
            "Empty first name should caused an Illegal Argument Exception");

    assertEquals("user's first name cannot be blank", thrown.getMessage());
  }

  @Test
  @DisplayName("Attempt to create user with empty email address")
  public void test_cannotCreateUserWithEmptyEmail() throws IllegalAccessException {
    String userEmail = "";
    IllegalArgumentException thrown =
        assertThrows(
            IllegalArgumentException.class,
            () -> {
              userSample.createUser(firstName, userEmail, randomGender(), randomStatus());
            },
            "Empty email address should caused an Illegal Argument Exception");

    assertEquals("user's email address cannot be blank", thrown.getMessage());
  }

  @Test
  @DisplayName("If save() method causes RuntimeException - UserServiceException is thrown")
  public void test_cannotCreateUserWithException() {

    when(userRepository.save(any(User.class))).thenThrow(RuntimeException.class);
    assertThrows(
        UserServiceException.class,
        () -> {
          userSample.createUser(firstName, userEmail, randomGender(), randomStatus());
        },
        "Should have throw UserServiceException");
  }

  @Test
  @DisplayName("Email notification exception is handled")
  public void test_createUserWithEmailNotificationException() {
    // Arrange
    when(userRepository.save(any(User.class))).thenReturn(true);

    doThrow(EmailNotificationServiceException.class)
        .when(emailVerificationService)
        .scheduleEmailConfirmation(any(User.class));

    // Act && Assert
    assertThrows(
        UserServiceException.class,
        () -> {
          userSample.createUser(firstName, userEmail, randomGender(), randomStatus());
        },
        "Should have thrown UserServiceException");

    verify(emailVerificationService, times(1)).scheduleEmailConfirmation(any(User.class));
  }

  @Test
  @DisplayName("Schedule email confirmation is executed")
  public void test_canCreateUserWithScheduledEmailConfirmation() throws IllegalAccessException {
    UserEndpoint userAPI = new UserEndpoint();

    // Arrange
    when(userRepository.save(any(User.class))).thenReturn(true);

    doCallRealMethod().when(emailVerificationService).scheduleEmailConfirmation(any(User.class));

    // Act
    userSample.createUser(firstName, userEmail, randomGender(), randomStatus());

    verify(emailVerificationService, times(1)).scheduleEmailConfirmation(any(User.class));
  }
}
