package users;

import static io.restassured.RestAssured.given;

import Enum.Gender;
import Enum.Status;
import authorization.Endpoints;
import io.qameta.allure.Step;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;
import java.util.List;
import java.util.Random;
import logs.Log;
import model.Data;
import model.RestClient;
import model.User;
import model.UserRequest;
import model.UsersResponse;
import model.service.UserService;

public class UserEndpoint implements UserService {

  @Override
  public User createUser(
      String firstName, String userEmail, String randomGender, String randomStatus) {
    return new User(firstName, userEmail, randomGender, randomStatus);
  }

  private static final List<Gender> VALUES_GENDER = List.of(Gender.values());
  private static final List<Status> VALUES_STATUS = List.of(Status.values());

  public static String randomGender() {
    return String.valueOf(VALUES_GENDER.get(new Random().nextInt(VALUES_GENDER.size())));
  }

  public static String randomStatus() {
    return String.valueOf(VALUES_STATUS.get(new Random().nextInt(VALUES_STATUS.size())));
  }

  private final RequestSpecification requestSpec =
      new RestClient().withUri(Endpoints.getUSERS()).build();

  @Step
  public List<Data> getUsers() {
    Log.info("Get users list");
    return given()
        .spec(requestSpec)
        .get()
        .then()
        .statusCode(200)
        .extract()
        .jsonPath()
        .getList("data", Data.class);
  }

  @Step("Create User {user.name}")
  public UsersResponse createUser(UserRequest user) {
    Log.info("Create a user");
    return given()
        .spec(requestSpec)
        .body(user)
        .post()
        .then()
        .statusCode(201)
        .extract()
        .body()
        .as(UsersResponse.class);
  }

  @Step
  public UsersResponse getUserById(int id) {
    Log.info("Get user by userId");
    return given()
        .spec(requestSpec)
        .get("/" + id)
        .then()
        .statusCode(200)
        .extract()
        .body()
        .as(UsersResponse.class);
  }

  @Step
  public UsersResponse updateUser(int id, UserRequest request) {
    Log.info("Update user");
    return given()
        .spec(requestSpec)
        .body(request)
        .put("/" + id)
        .then()
        .statusCode(200)
        .extract()
        .body()
        .as(UsersResponse.class);
  }

  @Step
  public UsersResponse patchUser(int id, UserRequest request) {
    Log.info("Patch user");
    return given()
        .spec(requestSpec)
        .body(request)
        .patch("/" + id)
        .then()
        .statusCode(200)
        .extract()
        .response()
        .as(UsersResponse.class);
  }

  @Step
  public ValidatableResponse deleteUser(int id) {
    Log.info("Delete user");
    return given().spec(requestSpec).delete("/" + id).then().statusCode(204);
  }
}
