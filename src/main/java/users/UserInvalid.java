package users;

import static io.restassured.RestAssured.given;

import authorization.Endpoints;
import io.qameta.allure.Step;
import io.restassured.specification.RequestSpecification;
import logs.Log;
import model.ResponseError;
import model.RestClient;
import model.User;
import model.UserRequest;
import model.service.UserService;

public class UserInvalid implements UserService {

  @Override
  public User createUser(
      String firstName, String userEmail, String randomGender, String randomStatus)throws IllegalAccessException {

    if (firstName == null || firstName.trim().length() == 0) {
      throw new IllegalAccessException("User name is empty");
    }

    return new User(firstName, userEmail, randomGender, randomStatus);
  }

  private final RequestSpecification requestSpec =
      new RestClient().withUri(Endpoints.getUSERS()).build();

  @Step("Invalid request")
  public ResponseError attemptToCreateUser(UserRequest user) {
    Log.info("Attempt to create a user");
    given().spec(requestSpec).body(user).post().then().extract().body().as(ResponseError.class);
    return new ResponseError();
  }
}
