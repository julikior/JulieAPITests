package model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_ABSENT)
public class UserRequest {

  public static String userEmail = "test.email" + System.currentTimeMillis() + "@15ce.com";
  public static String userName = "test_name_" + System.currentTimeMillis();

  private String name;
  private String gender;
  private String email;
  private String status;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getGender() {
    return gender;
  }

  public void setGender(String gender) {
    this.gender = gender;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public UserRequest(String name, String gender, String email, String status) {
    this.name = name;
    this.gender = gender;
    this.email = email;
    this.status = status;
  }
}
