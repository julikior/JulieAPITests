package model;

import java.util.HashMap;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class User {
  private final Map<String, Object> meta = new HashMap<>();
  private int id;
  private String name;
  private String gender;
  private String email;
  private String status;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public String getGender() {
    return gender;
  }

  public String getEmail() {
    return email;
  }

  public String getStatus() {
    return status;
  }

  public User(String name, String gender, String email, String status) {
    this.name = name;
    this.gender = gender;
    this.email = email;
    this.status = status;
  }
}
