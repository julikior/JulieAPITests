package model.service;

import model.User;

public interface UserService {
  User createUser(String firstName, String userEmail, String randomGender, String randomStatus)
      throws IllegalAccessException;
}
