package model.service;

import java.util.*;
import mock.UserRepository;
import model.User;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

  UserRepository userRepository;
  EmailVerificationService emailVerificationService;
  Map<String, User> userMap;

  public UserServiceImpl(
      UserRepository userRepository, EmailVerificationService emailVerificationService) {
    this.userRepository = userRepository;
    this.emailVerificationService = emailVerificationService;
  }

  @Override
  public User createUser(
      String firstName, String userEmail, String randomGender, String randomStatus)
      throws IllegalAccessException {

    if (firstName == null || firstName.trim().length() == 0) {
      throw new IllegalArgumentException("user's first name cannot be blank");
    }

    if (userEmail == null || userEmail.trim().length() == 0) {
      throw new IllegalArgumentException("user's email address cannot be blank");
    }

    User sampleUser = new User(firstName, randomGender, userEmail, randomStatus);

    boolean isUserCreated;
    try {
      isUserCreated = userRepository.save(sampleUser);
    } catch (RuntimeException e) {
      throw new UserServiceException(e.getMessage());
    }

    if (!isUserCreated) throw new UserServiceException("The user was not created");

    try {
      emailVerificationService.scheduleEmailConfirmation(sampleUser);
    } catch (RuntimeException e) {
      throw new UserServiceException(e.getMessage());
    }

    return sampleUser;
  }

  public UserServiceImpl() {
    userMap = new HashMap<>();
  }

  public User saveUser(User user) {
    userMap.put(String.valueOf(user.getId()), user);
    return userMap.get(String.valueOf(user.getId()));
  }

  public List<User> getUsers() {
    return new ArrayList<>(userMap.values());
  }

  public Optional<User> getUser(String id) {
    return Optional.ofNullable(userMap.get(id));
  }

  public void deleteUser(String id) {
    userMap.remove(id);
  }
}
