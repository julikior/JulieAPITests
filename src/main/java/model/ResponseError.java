package model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({"meta", "data"})
public class ResponseError extends ResponseDataError {

  private Object meta;
  public Data data;

  public Object getMeta() {
    return meta;
  }

  public Data getData() {
    return data;
  }

  public ResponseError(String field, String message, Object meta, Data data) {
    super(field, message);
    this.meta = meta;
    this.data = data;
  }

  public ResponseError() {}

  @Override
  public String toString() {
    return "model.ResponseError{" + "meta=" + meta + ", data=" + data + '}';
  }
}
