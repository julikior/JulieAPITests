package model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ResponseDataError {
  private String field;
  private String message;

  public ResponseDataError(String field, String message) {
    this.field = field;
    this.message = message;
  }

  public String getField() {
    return field;
  }

  public String getMessage() {
    return message;
  }

  public ResponseDataError() {}

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    ResponseDataError that = (ResponseDataError) o;

    if (!field.equals(that.field)) return false;
    return message.equals(that.message);
  }

  @Override
  public int hashCode() {
    int result = field.hashCode();
    result = 31 * result + message.hashCode();
    return result;
  }

  @Override
  public String toString() {
    return "model.ResponseDataError{"
        + "field='"
        + field
        + '\''
        + ", message='"
        + message
        + '\''
        + '}';
  }
}
