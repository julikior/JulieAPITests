package mock;

import java.util.HashMap;
import java.util.Map;
import model.User;

public class UserRepositoryImpl implements UserRepository {

  Map<String, User> usersSample = new HashMap<>();

  @Override
  public boolean save(User user) {

    boolean returnValue = false;

    if (!usersSample.containsKey(user.getId())) {
      usersSample.put(String.valueOf(user.getId()), user);
      returnValue = true;
    }

    return returnValue;
  }
}
