package mock;

import model.User;

public interface UserRepository {

  boolean save(User user);
}
